module gitlab.com/Renato776/login

go 1.13

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/lib/pq v1.8.0
)
