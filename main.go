package main

import (
	"os"
	"fmt"
	"errors"
	"database/sql"
	"context"

        "github.com/aws/aws-lambda-go/lambda"
	"github.com/gomodule/redigo/redis"
	_ "github.com/lib/pq"
)

type LoginReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginRes struct {
	Token string `json:"token"`
	Primary []string `json:"primary"`
	Secondary []string `json:"secondary"`
	Tables []string `json:"tables"`
}

var psqlInfo string
var db *sql.DB
var conn redis.Conn
var err error

func close_old_sessions(id string){
	//We close all opened sessions in Redis for user with ID id.
	//There's no need to close them in Pg cause that's done
	//automatically. Also, it could happen that we don't 
	//need to close any session to begin with.
	o_s, bp := db.Query("select id from session where active and "+
	"\"usuarioId\" = "+id)
	if bp != nil { return } //No opened sessions!
	defer o_s.Close()
	for o_s.Next() {
		var jk string
		bp = o_s.Scan(&jk)
		if bp != nil { 
			fmt.Println(bp)
			continue //We silently ignore the error.
			//Since I'm not sure what happened in this case.
		} 
		conn.Do("DEL", "core:"+jk)
	}
}
func validate_user(req LoginReq) (string) {
	//wrong user || wrong password || Number
	var id string
	var pass string
	err = db.QueryRow("select id,password from usuario"+
	" where username = $1;", req.Username).Scan(&id,&pass)
	if err != nil { return "wrong user" }
	if pass != req.Password { return "wrong pass" }
	return id
}
func login(id string) (string) {
	//We perform the actual user login, first in Pg and get the
	//token. Then we query Pg again for the initial information
	//and setting up the new Context in redis.
	var token string
	jk := "select * from usr_login("+id+");"
	err = db.QueryRow(jk).Scan(&token)
	if err != nil { panic (nil) } //Should never happen.

	//Alright, now let's setup the new Context on Redis!
	var stmt, module, version string
	jk = "select * from get_last("+token+");"
	err = db.QueryRow(jk).Scan(&stmt,&module,&version)
	if err != nil { panic (nil) }
	jm := "core:"+token
	fmt.Println(jm,id,stmt,module,version,token)
	conn.Do("HMSET", jm, "usr", id, "stmt", stmt, 
		"module", module, "version", version,
		"anchor", "r1", "token", token)
	return token
}
func extract_strings(q string) []string {
	vessel := make([]string, 0)
	cursor, bp := db.Query(q)
	if bp != nil { return vessel } //We return an empty array.
	defer cursor.Close()
	for cursor.Next() {
		var jk string
		bp = cursor.Scan(&jk)
		if bp != nil { continue } 
		vessel = append(vessel, jk)
	}
	return vessel
}
func Perform(ctx context.Context, req LoginReq) (LoginRes, error) {
	if db == nil || conn == nil { 
		return LoginRes{}, 
		errors.New("Connection to either database failed.") 
	}
	id := validate_user(req)
	if id == "wrong user" { 
		return LoginRes{}, errors.New("User does not exist") 
	} else if id == "wrong pass" {
		return LoginRes{}, errors.New("Password incorrect")
	}
	close_old_sessions(id)

	var answer LoginRes

	answer.Token = login(id)
	var sg string
	sg = "select button as b from user_buttons where usuario = "+id+" and not secondary order by orden"
	answer.Primary = extract_strings(sg)
	sg = "select button as b from user_buttons where usuario = "+id+" and secondary order by orden"
	answer.Secondary = extract_strings(sg)
	sg = "select v from visible_tables where u = "+id
	answer.Tables = extract_strings(sg)
	return answer, nil
}
func init(){
	psqlInfo = fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("PGHOST"), os.Getenv("PGPORT"), 
		os.Getenv("PGUSER"), os.Getenv("PGPASSWORD"),
		os.Getenv("PGDATABASE"))

	db,err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err) 
	}
	conn,err = redis.Dial("tcp",
	os.Getenv("REDIS_HOST")+":"+os.Getenv("REDIS_PORT"))
	if err != nil {
		panic(err)
	}
}
func terminar(){
	if db != nil {
		db.Close();
	}
	if conn != nil {
		conn.Close()
	}
}
func main() {
        lambda.Start(Perform)
	defer terminar()
}
